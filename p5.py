import sys, math, time, queue

__author__ = 'zow003@ucsd.edu, a91423168, ssioutis@ucsd.edu, a11508055'

"""
The main idea is based on the BFS algorithm that is demonstrated in the textbook.

The next potential state is generated one by one by changing one digit from the current processing
node and also check if it is a prime, if it is satisfied, then adding this path from the start to
this next potential node to the queue.

To speed up, I add one more check at each state, which is to check whether the generated next potential
prime is one digit differnt from the end prime, if so, construct the path and return, otherwise, continue.

I use explored_dict, a dictionary, to mark all the visited node to avoid circles, and frontier_list,
a list of possible possible pathes, where the path is also a list, to perform BFS.
"""

#Breadth First Search
def getPath(startStr, endStr):
    start = int(startStr)
    end = int(endStr)
    if start == end:
        return [start]

    numDigits = len(startStr)

    frontier_list = queue.PriorityQueue()
    # store frontier as [f score, node] tuples
    frontier_list.put((hamming_distance(startStr, endStr), [start]))

    explored_dict = {}
    explored_dict[start] = 0


    while not frontier_list.empty():
        frontier = frontier_list.get()
        curr = frontier[-1][0]
        currStr = str(curr)
        for i in range(numDigits - 1, -1, -1):
            for x in range(10):
                if i == 0 and x == 0:
                    continue
                if i == numDigits - 1 and numDigits > 1:
                    if x % 2 == 0 or x % 5 == 0:
                        continue
                if i == numDigits - 1 and numDigits == 1:
                    if x % 2 == 0 and x != 2:
                        continue
                child = int(currStr[:i] + str(x) + currStr[(i+1):])
                if child not in explored_dict and child not in frontier:
                    if isPrime(child):
                        childStr = str(child)
                        h_dist = hamming_distance(childStr, endStr)
                        path = frontier + (h_dist, [child])
                        if child == end:
                            return path
                        if len(childStr) <= len(endStr) and len(childStr) > 1:
                            diff = 0
                            for d in range(numDigits):
                                if childStr[d] != endStr[d]:
                                    diff += 1
                                    if diff > 1:
                                        break
                            if diff == 1 and d == len(endStr)-1:
                                # strip f scores from path
                                path_stripped = list(path[1::2])
                                path_stripped = [x[0] for x in path_stripped]
                                path_stripped.append(end)
                                return path_stripped

                        frontier_list.put(path)
                        explored_dict[child] = explored_dict.get(curr) + 1

    return []

# AKS method
def isPrime(n):
    if n < 2:
        return False
    if n == 2 or n == 3:
        return True
    elif not n & 1:
        return False
    if n % 3 == 0 or n % 2 == 0:
        return False
    for i in range(3, int(math.sqrt(n)) + 1, 2):
        if n % i == 0:
            return False
    return True

def hamming_distance(current, goal):
    dist = 0
    for idx, letter in enumerate(current):
        if letter != goal[idx]:
            dist += 1
    return dist

def main():
    f = open('output.txt', 'w')
    for line in sys.stdin:
        primes = str(line).split()

        start_time = time.time()
        path = getPath(primes[0], primes[1])
        print(time.time() - start_time)

        if len(path) == 0 or path[-1] != int(primes[1]):
            print("UNSOLVABLE\n")
        else:
            print(path)

        if len(path) == 0 or path[-1] != int(primes[1]):
            f.write('UNSOLVABLE\n');
        else:
            for p in path:
                f.write(str(p))
                f.write(' ')
            f.write('\n')
    f.close()

if __name__ == '__main__':
    main()