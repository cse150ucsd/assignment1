import sys, math, time, heapq, itertools

__author__ = 'zow003@ucsd.edu, a91423168, ssioutis@ucsd.edu, a11508055, ttedesch@ucsd.edu, a11155293'

def getHeuristic(x, y):
    return math.sqrt(sum(c1 != c2 for c1, c2 in zip(str(x), str(y))))

def getPath(startStr, endStr):    
    start = int(startStr)
    end = int(endStr)
    if not isPrime(start) or not isPrime(end):
        return []
    if start == end:
        return [start]
    
    #frontier_list = [[start]]
    pl = [start]
    w = len(pl)+getHeuristic(start,end)
    #print getHeuristic(start,end)
    frontier_list = [(w,[start])]
    explored_dict = {}
    explored_dict[start] = 0
    
    numDigits = len(startStr)
    #findSoln = False
    skip = False
    while len(frontier_list)>0:
        weight, frontier = heapq.heappop(frontier_list)
        #print weight
        #print frontier
        #print "frontier is %s" % frontier
        curr = frontier[-1]
        #explored_dict[curr] = 1
        currStr = str(curr)
        for i in range(numDigits - 1, -1, -1):
            for x in range(10):
                if i == 0 and x == 0:
                    continue
                if i == numDigits - 1 and x % 2 == 0:
                    continue
                curr_list = list(currStr)
                curr_list[i] = str(x)
                child = int(''.join(map(str,curr_list)))
                if isPrime(child):
                    if child not in explored_dict and child not in frontier:
                        path = []
                        path = frontier + [child]
                        #print "path is %s" % path
                        if child == end:
                            #findSoln = True
                            #print frontier
                            return path
                        childStr = str(child)
                        diff = 0
                        for d in range(numDigits):
                            if d == len(endStr): #!!! handling cases like 269 -> 13
                                return [] 
                            if childStr[d] != endStr[d]:
                                diff += 1
                                if diff > 1:
                                    break
                        if diff == 1 and d == len(endStr)-1:
                            #findSoln = True    
                            path.append(end)
                            return path
                        #frontier_list.append(path)
                        heapq.heappush(frontier_list, (len(path)+getHeuristic(child,end), path))
                        explored_dict[child] = explored_dict.get(curr) + 1
                 
    return []

# AKS method
def isPrime(n):
    if n < 2:
        return False
    if n == 2 or n == 3:
        return True
    elif not n & 1:
        return False
    if n % 3 == 0 or n % 2 == 0:
        return False
    for i in range(3, int(math.sqrt(n)) + 1, 2):
        if n % i == 0:
            return False
    return True

def main():
    f = open('output.txt', 'w')
    for line in sys.stdin:
        primes = str(line).split()

        path = getPath(primes[0], primes[1])
        
        if len(path) == 0 or path[-1] != int(primes[1]):
            print("UNSOLVABLE")
        else:
            print(" ".join(map(str, path)))
        
        if len(path) == 0 or path[-1] != int(primes[1]):
            f.write('UNSOLVABLE');
        else:
            for p in path:
                f.write(str(p))
                f.write(' ')
            f.write('\n')
    f.close()
    
if __name__ == '__main__':
    main()