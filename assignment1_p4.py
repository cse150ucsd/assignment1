import sys, math, time

__author__ = 'zow003@ucsd.edu, a91423168, ssioutis@ucsd.edu, a11508055, ttedesch@ucsd.edu, a11155293'

"""
Bidirectional Breadth First Search is based on Breadth First Search, 
but performing regular BFS on both end at the same time.

I use the same idea of BFS from Problem 1, but running from both start and end 
and I choose to proceed one level down from one end each time. For example,
both start and end are set as level 0, then I will construct level 1 from the start,
and after it is fully done, I will start level 1 from the end. So, by the time level 2 
from the start is processed, the end part will only have level 0 and level 1 available. 

Therefore, there are only two ways to reach the goal in this algorithm. One way is that
a node from the start part find the same node that appears at the same level from the end 
part. The other way is a node from the start part find the same node that has appeared at 
one level above from the end part. For example, a node on level 3 of the start graph can
find the same node that appears either on level 3 of the end graph or level 2 of the end 
graph.

I use 2 explored dictionaries to mark all the visited nodes reached from both ends separately 
to avoid circles, and 2 frontier_lists, each storing possible pathes from the its starting node, 
where path is also a list, to perform BFS from both ends. 
"""

#Bidirectional Breadth First Search
def getPath(startStr, endStr):    
    start = int(startStr)
    end = int(endStr)
    if not isPrime(start) or not isPrime(end):
        return []
    if start == end:
        print start
        print end
        return [start, end]
    
    frontier_list = [[start]]
    explored_dict = {}
    explored_dict[start] = [-1, 0]
    
    frontier_list2 = [[end]]
    explored_dict2 = {}
    explored_dict2[end] = [-1, 0]
    
    numDigits = len(startStr)
    numDigits2 = len(endStr)
    
    list1 = []
    list2 = []

    # really wish the instructions would have been more clear about this
    """
    if sum(c1 != c2 for c1, c2 in zip(startStr, endStr)) == 1:
        list1 = [start, start]
        list2 = [end, end]
        print(" ".join(map(str, list1)))
        print(" ".join(map(str, list2)))
        return [start, start, end, end]
    """

    findSoln = False
    while len(frontier_list) and len(frontier_list2):
        doTop = explored_dict[frontier_list[0][-1]][1] <= explored_dict2[frontier_list2[0][-1]][1]
        doBottom = not doTop
        if len(frontier_list) and doTop and not findSoln:
            frontier = frontier_list.pop(0)
            #print "frontier is %s" % frontier
            curr = frontier[-1]
            #explored_dict[curr] = 1
            currStr = str(curr)
            for i in range(numDigits - 1, -1, -1):
                for x in range(10):
                    if i == 0 and x == 0:
                        continue
                    if i == numDigits - 1 and x % 2 == 0:
                        continue
                    curr_list = list(currStr)
                    curr_list[i] = str(x)
                    child = int(''.join(curr_list))
                    if isPrime(child):
                        if child not in explored_dict and child not in frontier:
                            path = []
                            path = frontier + [child]
                            #print "path is %s" % path
                            if child in explored_dict2:
                                findSoln = True
                                list1 = path
                                break
                            frontier_list.append(path)
                            explored_dict[child] = [curr, explored_dict[curr][1] + 1]
                if findSoln:
                    #print list1
                    break     
            if not findSoln:
                continue        
        if len(frontier_list2) and doBottom or findSoln:
            frontier2 = frontier_list2.pop(0)
            #print frontier2
            curr2 = frontier2[-1]
            currStr2 = str(curr2)
            for i in range(numDigits2 - 1, -1, -1):
                for x in range(10):
                    if i == 0 and x == 0:
                        continue
                    if i == numDigits2 - 1 and x % 2 == 0:
                        continue
                    curr_list2 = list(currStr2)
                    curr_list2[i] = str(x)
                    child = int(''.join(curr_list2))
                    if isPrime(child):
                        if child not in explored_dict2 and child not in frontier2:
                            path = []
                            path = frontier2 + [child]
                            #print "path is %s" % path
                            if findSoln and curr2 == list1[-1]:
                                if curr2 == end:
                                    path = frontier2 + [start]
                                list2 = path
                                #print "case1"
                                print(" ".join(map(str, list1)))
                                print(" ".join(map(str, list2)))
                                return list1 + list2
                            if not findSoln and child in explored_dict:
                                findSoln = True
                                list2 = path
                                list1 = []
                                list1.append(child)
                                parent = explored_dict[child][0]
                                while parent != -1:
                                    list1.append(parent)
                                    parent = explored_dict[parent][0]
                                list1.reverse()
                                #print "case2"
                                print(" ".join(map(str, list1)))
                                print(" ".join(map(str, list2)))
                                return list1 + list2
                            frontier_list2.append(path)
                            explored_dict2[child] = [curr2, explored_dict2[curr2][1] + 1]
                    
    return []

def isPrime(n):
    if n < 2:
        return False
    if n == 2 or n == 3:
        return True
    elif not n & 1:
        return False
    if n % 3 == 0 or n % 2 == 0:
        return False
    for i in range(3, int(math.sqrt(n)) + 1, 2):
        if n % i == 0:
            return False
    return True

def main():
    f = open('output.txt', 'w')
    for line in sys.stdin:
        primes = str(line).split()

        path = getPath(primes[0], primes[1])
        
        #if len(path) == 0:
        #    print("UNSOLVABLE")
        
        if len(path) == 0:
            f.write('UNSOLVABLE');
        else:
            i = 1
            for p in path:
                f.write(str(p))
                f.write(' ')
                if len(path) <= 2:
                    f.write('\n')
                elif i == len(path) / 2:
                    f.write('\n')
            i += 1
    f.close()
    
if __name__ == '__main__':
    main()