import sys, time, math

__author__ = 'zow003@ucsd.edu, a91423168, ssioutis@ucsd.edu, a11508055, ttedesch@ucsd.edu, a11155293'

# global variables
path = []
visited = []

# find shortest path between start prime and end prime
# may use primes outside the range of [start, end]
# start_str and end_str must have the same length
def getPath(startStr, endStr):
    del path[:]
    del visited[:]
    start = int(startStr)
    end = int(endStr)
    if start == end:
        return [start]
    numDigits = len(startStr)

    limit = 8
    return iterativeDeepening(startStr, endStr, limit, numDigits)

# iterative deepening
def iterativeDeepening(start, end, limit, length):
    global path
    for L in range(0, limit + 1):
        # resize path, clear visited list
        path = [0] * (limit + 1)
        del visited[:]
        if recursiveDLS(start, end, L, length):
            path_trunc = [int(p) for p in path if p != 0]
            return path_trunc

    return []

# recursive depth limited search
def recursiveDLS(startStr, endStr, limit, length):
    global visited, path

    if startStr in visited:
        return False
    visited.append(startStr)

    if len(startStr) != len(endStr):
        return False
    # whether solution is adjacent node found via hamming distance
    if sum(c1 != c2 for c1, c2 in zip(startStr, endStr)) == 1:
        path.reverse()
        path.append(startStr)
        path.append(endStr)
        return True

    if limit == 0:
        return False
    # update path sequence
    if startStr not in path:
        path[limit] = startStr

    for i in range(length - 1, -1, -1):
        for x in range(10):
            if i == 0 and x == 0:
                continue
            if i == length - 1 and (x % 2 == 0 or x % 5 == 0):
                continue
            # modify the ith digit in startStr until a prime is found
            childStr = startStr[:i] + str(x) + startStr[(i+1):]
            child = int(childStr)

            # until a prime is found
            if isPrime(child):
                if recursiveDLS(childStr, endStr, limit-1, length):
                    return True
                if childStr in visited:
                    visited.remove(childStr)
    return False

# AKS method
def isPrime(n):
    if n < 2:
        return False
    if n == 2 or n == 3:
        return True
    elif not n & 1:
        return False
    if n % 3 == 0 or n % 2 == 0:
        return False
    for i in range(3, int(math.sqrt(n)) + 1, 2):
        if n % i == 0:
            return False
    return True

def main():
    f = open('output.txt', 'w')
    for line in sys.stdin:
        primes = str(line).split()

        # start_time = time.time()
        path = getPath(primes[0], primes[1])
        # print(time.time() - start_time)

        if len(path) == 0 or path[-1] != int(primes[1]):
            print("UNSOLVABLE\n")
        else:
            print(" ".join(map(str, path)))

        if len(path) == 0 or path[-1] != int(primes[1]):
            f.write('UNSOLVABLE\n');
        else:
            for p in path:
                f.write(str(p))
                f.write(' ')
            f.write('\n')
    f.close()

if __name__ == '__main__':
    main()             