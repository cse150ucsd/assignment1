import sys, math, time

__author__ = 'zow003@ucsd.edu, a91423168'

"""
The main idea is based on the BFS algorithm that is demonstrated in the textbook. 

The next potential state is generated one by one by changing one digit from the current processing 
node and also check if it is a prime, if it is satisfied, then adding this path from the start to 
this next potential node to the queue.

To speed up, I add one more check at each state, which is to check whether the generated next potential 
prime is one digit differnt from the end prime, if so, construct the path and return, otherwise, continue.

I use explored_dict, a dictionary, to mark all the visited node to avoid circles, and frontier_list,
a list of possible possible pathes, where the path is also a list, to perform BFS. 
"""

#Breadth First Search
def getPath(startStr, endStr):    
    start = int(startStr)
    end = int(endStr)
    if not isPrime(start) or not isPrime(end):
        return []
    if start == end:
        return [start]
    
    frontier_list = [[start]]
    explored_dict = {}
    explored_dict[start] = 0
    
    numDigits = len(startStr)
    #findSoln = False
    while len(frontier_list) > 0:
        frontier = frontier_list.pop(0)
        #print "frontier is %s" % frontier
        curr = frontier[-1]
        #explored_dict[curr] = 1
        currStr = str(curr)
        for i in xrange(numDigits - 1, -1, -1):
            for x in xrange(10):
                if i == 0 and x == 0:
                    continue
                if i == numDigits - 1 and x % 2 == 0:
                    continue
                curr_list = list(currStr)
                curr_list[i] = str(x)
                child = int(''.join(curr_list))
                if isPrime(child):
                    if explored_dict.has_key(child) == False and child not in frontier:
                        path = []
                        path = frontier + [child]
                        #print "path is %s" % path
                        if child == end:
                            #findSoln = True
                            #print frontier
                            return path
                        childStr = str(child)
                        diff = 0
                        for d in xrange(numDigits):
                            if childStr[d] != endStr[d]:
                                diff += 1
                                if diff > 1:
                                    break
                        if diff == 1 and d == len(endStr)-1:
                            #findSoln = True    
                            path.append(end)
                            return path
                        frontier_list.append(path)
                        explored_dict[child] = explored_dict.get(curr)  + 1
                 
    return []

def isPrime(n):
    if n < 2:
        return False
    elif not n & 1: 
        return False
    for i in range(2, int(math.sqrt(n)) + 1):
        if n % i == 0:
            return False
    return True

def main():
    f = open('output.txt', 'w')
    for line in sys.stdin:
        primes = str(line).split()
        
        start_time = time.time()
        path = getPath(primes[0], primes[1])
        print(time.time() - start_time)
        
        if len(path) == 0 or path[-1] != int(primes[1]):
            print("UNSOLVABLE")
        else:
            print path
        
        if len(path) == 0 or path[-1] != int(primes[1]):
            f.write('UNSOLVABLE');
        else:
            for p in path:
                f.write(str(p))
                f.write(' ')
            f.write('\n')
    f.close()
    
if __name__ == '__main__':
    main()