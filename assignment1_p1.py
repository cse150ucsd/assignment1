import sys, math, time

__author__ = 'zow003@ucsd.edu, a91423168, ssioutis@ucsd.edu, a11508055, ttedesch@ucsd.edu, a11155293'


#Breadth First Search
def getPath(startStr, endStr):    
    start = int(startStr)
    end = int(endStr)
    if start == end:
        return [start]

    numDigits = len(startStr)

    frontier_list = [[start]]
    explored_dict = {}
    explored_dict[start] = 0

    while len(frontier_list) > 0:
        frontier = frontier_list.pop(0)
        curr = frontier[-1]
        currStr = str(curr)
        for i in range(numDigits - 1, -1, -1):
            for x in range(10):
                if i == 0 and x == 0:
                    continue
                if i == numDigits - 1 and numDigits > 1:
                    if x % 2 == 0 or x % 5 == 0:
                        continue
                if i == numDigits - 1 and numDigits == 1:
                    if x % 2 == 0 and x != 2:
                        continue
                child = int(currStr[:i] + str(x) + currStr[(i+1):])
                if child not in explored_dict and child not in frontier:
                    if isPrime(child):
                        path = frontier + [child]
                        if child == end:
                            return path
                        childStr = str(child)
                        if len(childStr) <= len(endStr) and len(childStr) > 1:
                            diff = 0
                            for d in range(numDigits):
                                if childStr[d] != endStr[d]:
                                    diff += 1
                                    if diff > 1:
                                        break
                            if diff == 1 and d == len(endStr)-1:
                                path.append(end)
                                return path

                        frontier_list.append(path)
                        explored_dict[child] = explored_dict.get(curr) + 1
                 
    return []

# AKS method
def isPrime(n):
    if n < 2:
        return False
    if n == 2 or n == 3:
        return True
    elif not n & 1:
        return False
    if n % 3 == 0 or n % 2 == 0:
        return False
    for i in range(3, int(math.sqrt(n)) + 1, 2):
        if n % i == 0:
            return False
    return True

def main():
    f = open('output.txt', 'w')
    for line in sys.stdin:
        primes = str(line).split()

        # start_time = time.time()
        path = getPath(primes[0], primes[1])
        # print(time.time() - start_time)
        
        if len(path) == 0 or path[-1] != int(primes[1]):
            print("UNSOLVABLE\n")
        else:
            print(" ".join(map(str, path)))
        
        if len(path) == 0 or path[-1] != int(primes[1]):
            f.write('UNSOLVABLE\n');
        else:
            for p in path:
                f.write(str(p))
                f.write(' ')
            f.write('\n')
    f.close()
    
if __name__ == '__main__':
    main()